import * as React from 'react';
import Label from '../label/Label';

interface SelectProps{
  values?: string[],
  name: string,
  value: string,
  title?: string,
  onChange?: (name: string, value: string) => void,
};

const Select: React.SFC<SelectProps> = (props) => {
  const {onChange, values, title, ...vendorProps} = props;
  return (
    <div className="form-group">
      <Label name={props.name}>{title}</Label>
      <select
        className="form-control"
        onChange={e => props.onChange(props.name, e.target.value)}
        {...vendorProps}
      >
        {props.values.map(value => (
          <option value={value} key={value}>
            {value}
          </option>
        ))}
      </select>
    </div>
  );
};

Select.defaultProps = {
  values: [],
  onChange: () => {},
  title: null,
};

export default Select;
