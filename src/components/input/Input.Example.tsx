import * as React from 'react';
import './Input.Example.styl';

interface InputExampleProps {
  values?: string[];
  onChange?: (value: string) => void;
}

const InputExample: React.SFC<InputExampleProps> = props => {
  if (!props.values) {
    return null;
  }
  return (
    <React.Fragment>
      {props.values.map(value => (
        <button
          className="input__example btn btn-primary"
          onClick={() => {
            props.onChange(value);
          }}
          key={value}
        >
          {value}
        </button>
      ))}
    </React.Fragment>
  );
};

InputExample.defaultProps = {
  onChange: () => {},
};

export default InputExample;
