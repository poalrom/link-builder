import * as React from 'react';
import Label from '../label/Label';
import Example from './Input.Example';
import Addon from './Input.Addon';

export interface InputProps {
  onChange?: (name: string, value: string) => void;
  name: string;
  title?: string;
  value: string;
  prepend?: string | JSX.Element;
  append?: string | JSX.Element;
  size?: string;
  exampleValues?: string[];
  [propName: string]: any;
}

const Input: React.SFC<InputProps> = props => {
  const { title, prepend, append, size, exampleValues, onChange, ...inputProps } = props;
  return (
    <div className={`form-group ${props.className || ''}`}>
      <Label name={props.name}>{title}</Label>
      <div className={`input-group input-group-${size} ${exampleValues ? 'mb-1' : 'mb3'}`}>
        <Addon type="prepend">{prepend}</Addon>
        <input
          {...inputProps}
          className="form-control"
          type="text"
          onChange={e => {
            onChange(props.name, e.target.value);
          }}
        />
        <Addon type="append">{append}</Addon>
      </div>
      <Example
        values={exampleValues}
        onChange={value => {
          onChange(props.name, value);
        }}
      />
    </div>
  );
};

Input.defaultProps = {
  onChange: () => {},
  title: null,
  prepend: null,
  append: null,
  size: 'md',
  exampleValues: null,
};

export default Input;
