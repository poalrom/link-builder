import * as React from 'react';

interface InputAddonProps {
  children?: JSX.Element | string;
  type: 'prepend' | 'append'
}

const InputAddon: React.SFC<InputAddonProps> = props => {
  if (!props.children) {
    return null;
  }
  return (
    <div className={`input-group-${props.type}`}>
      {props.children}
    </div>
  );
};

export default InputAddon;
