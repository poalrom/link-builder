import * as React from 'react';

type SwitchProps = {
  name: string,
  title?: string,
  onChange?: (name: string, value: boolean) => void,
  checked?: boolean
};

const Switch: React.SFC<SwitchProps> = (props) => (
  <div className="btn-group-toggle" data-toggle="buttons" style={{ marginTop: 29 }}>
    <label className={`btn btn-block ${props.checked ? 'btn-success active' : 'btn-secondary'}`}>
      <input
        type="checkbox"
        onChange={e => props.onChange(props.name, e.target.checked)}
      />{' '}
      {props.title}
    </label>
  </div>
);

Switch.defaultProps = {
  onChange: () => {},
  title: null,
  checked: false
};

export default Switch;
