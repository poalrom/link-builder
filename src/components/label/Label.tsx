import * as React from 'react';

interface LabelProps {
  name: string;
  children?: JSX.Element | string;
}

const Label = (props: LabelProps) => {
  if (!props.children) {
    return null;
  }
  return <label htmlFor={props.name}>{props.children}</label>;
};

export default Label
