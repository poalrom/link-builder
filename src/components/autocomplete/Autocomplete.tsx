import * as React from 'react';
import Input, { InputProps } from '../input/Input';
import AutocompleteList from './Autocomplete.List';
import './Autocomplete.styl';

interface AutocompleteProps extends InputProps {
  values?: string[];
}

interface AutocompleteState {
  openList: boolean;
  focusedIndex: number;
  matchedValues: string[];
}

class Autocomplete extends React.Component<AutocompleteProps, AutocompleteState> {
  constructor(props: AutocompleteProps) {
    super(props);
    this.state = {
      openList: false,
      focusedIndex: 0,
      matchedValues: props.values,
    };
    document.addEventListener('click', e => {
      if (
        !(e.target as HTMLElement).classList.contains('form-control') &&
        !(e.target as HTMLElement).closest('.autocomplete__list')
      ) {
        this.closeList();
      }
    });
  }

  componentDidUpdate() {
    if (this.state.focusedIndex !== 0) {
      const filteredValues = this.filterValues();
      if (this.state.focusedIndex >= filteredValues.length) {
        this.restoreFocusedIndex();
      }
    }
  }

  openList = () => {
    this.setState({ openList: true });
  };

  closeList = () => {
    this.setState({ openList: false });
  };

  selectValue = (value: string) => {
    this.props.onChange(this.props.name, value);
    this.closeList();
  };

  restoreFocusedIndex = () => {
    this.setState({ focusedIndex: 0 });
  };

  handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (!this.state.openList) {
      this.openList();
    }
    switch (event.key) {
      case 'Enter':
        this.selectValue(this.props.values[this.state.focusedIndex]);
        event.preventDefault();
        break;
      case 'ArrowUp':
        if (this.state.focusedIndex > 0) {
          this.setState({ focusedIndex: this.state.focusedIndex - 1 });
          event.preventDefault();
        }
        break;
      case 'ArrowDown':
        if (this.state.focusedIndex < this.props.values.length - 1) {
          this.setState({ focusedIndex: this.state.focusedIndex + 1 });
          event.preventDefault();
        }
        break;
      default:
        break;
    }
  };

  filterValues = () => {
    return this.props.values.filter(value => value.search(this.props.value) !== -1);
  };

  render() {
    const { values, ...inputProps } = this.props;
    return (
      <div className="autocomplete" onFocus={this.openList} onKeyDown={this.handleKeyPress}>
        <Input {...inputProps} className="autocomplete__input" />
        {inputProps.value.length > 0 &&
          values &&
          this.state.openList && (
            <AutocompleteList
              value={this.props.value}
              values={this.filterValues()}
              focusedIndex={this.state.focusedIndex}
              onSelect={this.selectValue}
            />
          )}
      </div>
    );
  }
}

export default Autocomplete;
