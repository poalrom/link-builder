import * as React from 'react';

interface AutocompleteListProps {
  value: string;
  values: string[];
  onSelect: (value: string) => void;
  focusedIndex: number;
}

const AutocompleteList = (props: AutocompleteListProps) => {
  return (
    <div className="list-group autocomplete__list">
      {props.values.map((value, i) => (
        <button
          type="button"
          className={`list-group-item list-group-item-action ${props.focusedIndex === i ? 'active' : ''}`}
          key={i}
          onClick={() => {
            props.onSelect(value);
          }}
        >
          {value}
        </button>
      ))}
    </div>
  );
};

export default AutocompleteList;
