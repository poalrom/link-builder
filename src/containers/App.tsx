// @flow

import * as React from 'react';
import { HashRouter as BaseRouter, NavLink, Route, Switch } from 'react-router-dom';
import Ozd, { OzdConfig } from './Ozd';

interface AppState {
  config?: {
    ozd: OzdConfig,
  };
}

class App extends React.Component {
  state: AppState = {
    config: null,
  };

  componentDidMount() {
    fetch('/config.json')
      .then(res => res.json())
      .then(data => this.setState({ config: data }))
      .catch(() => {
        alert('Не могу загрузить конфиг. Позови Алёшку');
      });
  }

  render() {
    if (!this.state.config) {
      return null;
    }
    return (
      <BaseRouter>
        <React.Fragment>
          <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <span className="navbar-brand col-sm-3 col-md-2 mr-0">Url builder</span>
          </nav>
          <div className="container-fluid">
            <div className="row">
              <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                <div className="sidebar-sticky">
                  <ul className="nav flex-column">
                    <li className="nav-item">
                      <NavLink to="/ozd" className="nav-link" activeClassName="active">
                        Ссылки в ОЗД
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink to="/universal" className="nav-link" activeClassName="active">
                        Универсальные ссылки
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </nav>
              <main className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <Switch>
                  <Route path="/ozd" render={() => <Ozd config={this.state.config.ozd} />} />
                </Switch>
              </main>
            </div>
          </div>
        </React.Fragment>
      </BaseRouter>
    );
  }
}

export default App;
