// @flow

import * as React from 'react';
import { QRCode } from 'react-qr-svg';
import Autocomplete from '../components/autocomplete/Autocomplete';
import Input from '../components/input/Input';
import Select from '../components/select/Select';
import Switch from '../components/switch/Switch';
import * as validUrl from 'valid-url';
import * as copy from 'copy-to-clipboard';

interface DocInterface {
  base: string;
  n: string;
  donly: boolean;
  attachment: boolean;
}

export interface OzdConfig {
  hosts: string[];
  exampleValues: {
    [key: string]: string[];
  };
}

interface OzdStateInterface {
  host: string;
  req: string;
  service: string;
  ric: string;
  distr: string;
  delay: string;
  complect: string;
  docs: DocInterface[];
}

const reqValues = ['doc', 'ivdoc'];
const serviceValues = ['nav', 'aa2', 'default'];
const complectValues = ['cons', 'regbase', 'bb0'];
const defaultDoc: DocInterface = {
  base: '',
  n: '',
  donly: false,
  attachment: false,
};

class Ozd extends React.Component<{ config: OzdConfig }, OzdStateInterface> {
  state: OzdStateInterface = {
    host: '',
    req: reqValues[0],
    service: serviceValues[0],
    ric: '',
    distr: '',
    delay: '',
    complect: complectValues[0],
    docs: [
      {
        ...defaultDoc,
      },
    ],
  };

  buildUrl = () => {
    let url = this.state.host.match(/https?:\/\//g) ? this.state.host : `http://${this.state.host}`;
    url += `?req=${this.state.req};service=${this.state.service};ric=${this.state.ric};distr=${this.state.distr};`;
    if (this.state.req === 'ivdoc') {
      url += this.state.complect ? `complect=${this.state.complect};` : '';
      url += this.state.delay ? `delay=${this.state.delay};` : '';
    }
    url += this.state.docs
      .map(
        doc =>
          `base=${doc.base};n=${doc.n}${doc.donly ? ';donly=1' : ''}${doc.attachment ? ';download=attachment' : ''}`,
      )
      .join(';');
    return url;
  };

  handleInput = (name: any, value: string) => {
    this.setState({ [name]: value });
  };

  handleDocInput = (i: number, name: string, value: string | boolean) => {
    const newDocs = [...this.state.docs];
    newDocs[i] = { ...newDocs[i], [name]: value };
    this.setState({ docs: newDocs });
  };

  addDoc = () => {
    this.setState({ docs: [...this.state.docs, { ...defaultDoc }] });
  };

  removeDoc = (i: number) => {
    const newDocs = [...this.state.docs];
    newDocs.splice(i, 1);
    this.setState({ docs: newDocs });
  };

  renderDoc = (doc: DocInterface, i: number) => {
    const handleChange = (name: string, value: string | boolean) => this.handleDocInput(i, name, value);
    return (
      <div className="row" key={i}>
        <div className="col">
          <Input
            name="base"
            value={doc.base}
            onChange={handleChange}
            title="base"
            exampleValues={this.props.config.exampleValues.base}
          />
        </div>
        <div className="col">
          <Input
            name="n"
            value={doc.n}
            onChange={handleChange}
            title="n"
            exampleValues={this.props.config.exampleValues.n}
          />
        </div>
        <div className="col" style={{ flex: 0 }}>
          <Switch name="donly" onChange={handleChange} title="donly" checked={doc.donly} />
        </div>
        <div className="col" style={{ flex: 0 }}>
          <Switch name="attachment" onChange={handleChange} title="attachment" checked={doc.attachment} />
        </div>
        <div className={`col ${this.state.docs.length === 1 ? ' invisible' : ''}`} style={{ flex: 0 }}>
          <label>&nbsp;</label>
          <button type="button" className="btn btn-danger btn-block" onClick={() => this.removeDoc(i)}>
            Удалить документ
          </button>
        </div>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="h2 border-bottom pb-2 mb-3">Сбор ссылок на документы в ОЗД</h1>
        <div className="row">
          <div className="col">
            <Input
              name="url"
              value={this.buildUrl()}
              readOnly
              prepend={<span className="input-group-text">Url</span>}
              size="lg"
              append={
                <button
                  onClick={() => copy(this.buildUrl())}
                  disabled={!validUrl.isWebUri(this.buildUrl())}
                  className="btn btn-primary"
                >
                  Copy
                </button>
              }
            />
            <Autocomplete
              name="host"
              value={this.state.host}
              onChange={this.handleInput}
              title="host"
              values={this.props.config.hosts}
            />
          </div>
          <div className="col" style={{ flex: 0 }}>
            <QRCode value={this.buildUrl()} style={{ height: 131 }} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Select name="req" value={this.state.req} onChange={this.handleInput} title="req" values={reqValues} />
          </div>
          <div className="col">
            <Select
              name="service"
              value={this.state.service}
              onChange={this.handleInput}
              title="service"
              values={serviceValues}
            />
          </div>
          <div className="col">
            <Input
              name="ric"
              value={this.state.ric}
              onChange={this.handleInput}
              title="ric"
              exampleValues={this.props.config.exampleValues.ric}
            />
          </div>
          <div className="col">
            <Input
              name="distr"
              value={this.state.distr}
              onChange={this.handleInput}
              title="distr"
              exampleValues={this.props.config.exampleValues.distr}
            />
          </div>
          <div className={`col ${this.state.req !== 'ivdoc' ? ' d-none' : ''}`}>
            <Input name="delay" value={this.state.delay} onChange={this.handleInput} title="delay" />
          </div>
          <div className={`col ${this.state.req !== 'ivdoc' ? ' d-none' : ''}`}>
            <Select
              name="complect"
              value={this.state.complect}
              onChange={this.handleInput}
              title="complect"
              values={complectValues}
            />
          </div>
        </div>
        <h5>Документы</h5>
        {this.state.docs.map(this.renderDoc)}
        <div className="row">
          <div className="col">
            <button type="button" className="btn btn-success btn-lg btn-block" onClick={this.addDoc}>
              Добавить документ
            </button>
            <button
              type="button"
              className="btn btn-primary btn-lg btn-block"
              disabled={!validUrl.isWebUri(this.buildUrl())}
              onClick={() => {
                window.open(this.buildUrl());
              }}
            >
              Перейти
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Ozd;
