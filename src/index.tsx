// @flow

import * as React from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import App from './containers/App';
import './index.styl';

const rootElement = document.getElementById('root');
if (rootElement) {
  render(<App />, rootElement);
}
